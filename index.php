<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8" />
        <title>Upload</title>

        <!--jQuery-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <!--Bootstrap-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.4.1/css/bootstrap.min.css" />

        <!--Font-Awesome-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.min.css" integrity="sha256-zmfNZmXoNWBMemUOo1XUGFfc0ihGGLYdgtJS3KCr/l0=" crossorigin="anonymous" />

        <!--FileInput-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/css/all.min.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.3/css/fileinput.min.css" />
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.3/js/plugins/sortable.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/dompurify/1.0.10/purify.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.3/js/fileinput.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-fileinput/5.0.3/js/locales/es.min.js"></script>

    </head>
    <body>
        <form enctype="multipart/form-data">
            <h1>Prueba clase</h1>
            <?php
                // Requerido
                require_once("upload.files.php");

                // Instancia de la Clase
                $objInput = new uploadFiles();

                // Creación de los INPUT
                //$objInput->drawInput("paquetes", "45", "superbanner", ['jpg','png','gif'], 1, false); echo "<hr>";
                //$objInput->drawInput("paquetes", "45", "galeria", ['jpg','png','gif'], 12); echo "<hr>";
                $objInput->drawInput("paquetes", "45", "documentacion", NULL, 20); echo "<hr>";
                //$objInput->drawInput("paquetes", "45", "autores", ['jpg','png','gif'], 1, false);

                // Recupera los archivos cargados
                $archivos = $objInput->getFiles("paquetes", "45");
                var_dump($archivos);

                // Recolector de basura
                $objInput->trashCollector();
            ?>
        </form>
    </body>
</html>
