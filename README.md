# UPLOAD.files.php
Implementación de Bootstrap "fileinput" con la clase "upload.class.php" sin la utilización de Base de Datos.

Los archivos son cargados y administrados por el plugin.

Los archivos son cargados teniendo en cuenta tres parametros:
- Modulo (noticias, usuarios, inmuebles, etc)
- Id (identificador interno del sistema, un id de registro)
- Instancia (galería, banners, etc)

El plugin genera subcarpetas dentro de una carpeta madre de carga (ej: uploads/) con la siguiente estructura, donde almacena los archivos:
- uploads/noticias/45/galeria/*.jpg
- uploads/noticias/45/banners/*.jpg
- uploads/noticias/45/banners/*.pdf

Puede configurarse para almacenar mas de una copia de un archivo si es de tipo imagen.
Este proceso crea carpetas dentro de la "instancia" con el nombre elegido.
Por defecto, todas las imagenes cargadas crearán una copia con un formato comprimido dentro de la subcarpeta "thumbs".
Ejemplo:
- uploads/noticias/45/galeria/foto.jpg (original)
- uploads/noticias/45/galeria/thumbs/foto.jpg (copia con compresión)

Los archivos son ordenados internamente agregando como prefijo a cada uno de ellos un indice.
Los archivos nuevos son agregados siempre al final de la cola de archivos.

El ordenamiento de archivos esta realizado por AJAX pero no esta implementado de ninguna manera.

## Herramientas utilizadas
- https://getbootstrap.com/ (v3.x)
- http://plugins.krajee.com/file-input
- https://www.verot.net/php_class_upload.htm
