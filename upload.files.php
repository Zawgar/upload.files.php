<?php
    class uploadFiles {
        // CONFIGURACIÓN general del plugin Upload
        public $classUpload = "controller.files.php"; // este mismo archivo
        public $urlHost     = "http://localhost/UPLOAD.files.php/"; // raiz del sitio
        public $dirUpload   = "uploads/"; // carpeta en la que se van a cargar los archivos / raiz en $urlHost
        public $indexDigit  = 4; // cantidad de caracteres reservados para el indice de los archivos
        public $inputClass  = "inputFile"; // clase html que identifica a las etiquetas <input> que serán utilizadas

        // Array con la cantidad de imagenes que deben crearse de cada una que es cargada
        // el indice representa la carpeta en la que se debe guardar y, el array asociado, las opcione de la imagen
        private $defaultVersions = [
            ""              => false, // imagen original sin modificación
            "thumb" => [
                "image_convert"     => "jpg",
                "jpeg_quality"      => 50,
                "image_resize"      => true,
                "image_x"           => 100,
                "image_ratio_y"     => true
            ]
        ];

        const tipos_permitidos = ['jpeg', 'jpg', 'png', 'gif'];

        public function __construct(
            $classUpload    = "controller.files.php",
            $urlHost        = "http://localhost/UPLOAD.files.php/",
            $dirUpload      = "uploads/",
            $indexDigit     = 4,
            $inputClass     = "inputFile"
        ){
            $this->classUpload  = $classUpload;
            $this->urlHost      = $urlHost;
            $this->dirUpload    = $dirUpload;
            $this->indexDigit   = $indexDigit;
            $this->inputClass   = $inputClass;
        }

        /**
         * @param $modulo           nombre del módulo
         * @param null $id          nombre de la instancia
         * @param $instancia        id registro actual (correcpondiente al módulo)
         * @param array $format     listado con los formatos permitidos
         * @param int $max          cantidad máxima de archivos que se puedan cargar en esta instancia
         * @param bool $multiple    permite seleccionar multiples archivos para cargar
         * @param array $opciones   opciones varias del plugin
         *
         * @return array
         */
        public function drawInput($modulo, $id = null, $instancia, $format = self::tipos_permitidos, $max = 0, $multiple = true, $opciones = []){
            if(is_null($id)) $id = date("YmdHis");

            // busca los archivos ya cargados para cada instancia
            $dir = $this->dirUpload . $modulo . "/" . $id . "/" . $instancia;
            $filesPreview = $this->loadFiles($dir, [], $format);

            // Identificador del objeto INPUT
            $identification = $id . $instancia;

            // Preview de los archivos, forzado con iconos
            $previewFileIconSettings = [
                // Imagenes
                /*
                'jpg' => '<i class="fa fa-file-photo-o text-danger"></i>',
                'gif' => '<i class="fa fa-file-photo-o text-muted"></i>',
                'png' => '<i class="fa fa-file-photo-o text-primary"></i>',
                */

                // Documentos de texto
                'txt'   => '<i class="fa fa-file-text-o text-info"></i>',
                'docx'  => '<i class="fa fa-file-word-o text-primary"></i>',
                'doc'   => '<i class="fa fa-file-word-o text-primary"></i>',
                'rtf'   => '<i class="fa fa-file-word-o text-primary"></i>',
                'pdf'   => '<i class="fa fa-file-pdf-o text-danger"></i>',

                // Planillas de calculo
                'xlsx'  => '<i class="fa fa-file-excel-o text-success"></i>',
                'xls'   => '<i class="fa fa-file-excel-o text-success"></i>',

                // Presentaciones
                'ppt'   => '<i class="fa fa-file-powerpoint-o text-danger"></i>',
                'pptx'  => '<i class="fa fa-file-powerpoint-o text-danger"></i>',

                // Webs
                'htm'   => '<i class="fa fa-file-code-o text-info"></i>',
                'html'  => '<i class="fa fa-file-code-o text-info"></i>',
                'php'   => '<i class="fa fa-file-code-o text-info"></i>',

                // Video/Audio
                'mov'   => '<i class="fa fa-file-movie-o text-warning"></i>',
                'mp3'   => '<i class="fa fa-file-audio-o text-warning"></i>',
                'avi'   => '<i class="fa fa-file-movie-o text-warning"></i>',
                'mpg'   => '<i class="fa fa-file-movie-o text-warning"></i>',
                'mkv'   => '<i class="fa fa-file-movie-o text-warning"></i>',
                'm4a'   => '<i class="fa fa-file-movie-o text-warning"></i>',
                'mp4'   => '<i class="fa fa-file-movie-o text-warning"></i>',
                '3gp'   => '<i class="fa fa-file-movie-o text-warning"></i>',
                'webm'  => '<i class="fa fa-file-movie-o text-warning"></i>',
                'wmv'   => '<i class="fa fa-file-movie-o text-warning"></i>',

                // Archivos Comprimidos
                'zip'   => '<i class="fa fa-file-archive-o text-muted"></i>',
                'rar'   => '<i class="fa fa-file-archive-o text-muted"></i>',
                'tar'   => '<i class="fa fa-file-archive-o text-muted"></i>',
                'gzip'  => '<i class="fa fa-file-archive-o text-muted"></i>',
                'gz'    => '<i class="fa fa-file-archive-o text-muted"></i>',
                '7z'    => '<i class="fa fa-file-archive-o text-muted"></i>'
            ];

            $previewZoomButtonIcons = [
                "prev"          => '<i class="fas fa-caret-left"></i>',
                "next"          => '<i class="fas fa-caret-right"></i>',
                "toggleheader"  => '<i class="fas fa-arrows-alt-v"></i>',
                "fullscreen"    => '<i class="fas fa-expand-arrows-alt"></i>',
                "borderless"    => '<i class="fas fa-arrows-alt"></i>',
                "close"         => '<i class="fas fa-times"></i>'
            ];

            $previewZoomButtonClasses = [
                "prev"          => 'btn btn-navigate btn-xs',
                "next"          => 'btn btn-navigate btn-xs',
                "toggleheader"  => 'btn btn-default btn-header-toggle btn-xs',
                "fullscreen"    => 'btn btn-default btn-xs',
                "borderless"    => 'btn btn-default btn-xs',
                "close"         => 'btn btn-default btn-xs'
            ];

            $fileActionSettings=[
                "showRemove"    => (isset($opciones['fileActionSettings']['showRemove'])?$opciones['fileActionSettings']['showRemove']:true),
                "showUpload"    => (isset($opciones['fileActionSettings']['showUpload'])?$opciones['fileActionSettings']['showUpload']:false),
                "showDownload"  => (isset($opciones['fileActionSettings']['showDownload'])?$opciones['fileActionSettings']['showDownload']:true),
                "showZoom"      => (isset($opciones['fileActionSettings']['showZoom'])?$opciones['fileActionSettings']['showZoom']:true),
                "showDrag"      => (isset($opciones['fileActionSettings']['showDrag'])?$opciones['fileActionSettings']['showDrag']:true),

                "removeIcon" => '<i class="fas fa-trash-alt text-danger"></i>',
                "removeClass" => 'btn btn-kv btn-default btn-outline-secondary btn-xs',
                "removeErrorClass" => 'btn btn-kv btn-danger',

                "uploadIcon" => '<i class="fas fa-upload text-primary"></i>',
                "uploadClass" => 'btn btn-kv btn-default btn-outline-secondary btn-xs',
                "uploadRetryIcon" => '<i class="glyphicon glyphicon-repeat"></i>',

                "downloadIcon" => '<i class="fas fa-download text-primary"></i>',
                "downloadClass" => 'btn btn-default btn-outline-secondary btn-xs',

                "zoomIcon" => '<i class="fas fa-search-plus text-warning"></i>',
                "zoomClass" => 'btn btn-kv btn-default btn-outline-secondary btn-xs',

                "dragIcon" => '<i class="fas fa-arrows-alt" style="pointer-events: none;"></i>',
                "dragClass" => 'text-info',
                "dragSettings" => []
            ];

            // Información extra al realizar UPLOAD/DELETE
            $uploadExtraData = $deleteExtraData = [
                "uploadFile"    => true,
                "modulo"        => $modulo,
                "instancia"     => $instancia,
                "id"            => $id,
                "maxFiles"      => $max
            ];
            $uploadExtraData['accion'] = "add";
            $deleteExtraData['accion'] = "del";

            // ************************
            // HTML *******************
            // ************************
            $html = "<div class='form-group'>";
            $html .= "<input";
            $html .= " class='" . $this->inputClass . "'";
            $html .= " type='file'";
            $html .= " name='" . $identification . "[]'";
            if($multiple) $html .= " multiple";
            $html .= " data-language='es'";

            $html .= " data-show-caption='true'";
            $html .= " data-show-preview='true'";
            $html .= " data-show-close='false'"; // este parametro se deshabilita por defecto ya que causa un error
            $html .= " data-show-remove='false'"; // este parametro se deshabilita por defecto ya que causa un error
            $html .= " data-show-upload='true'";
            $html .= " data-show-cancel='true'";
            $html .= " data-show-uploaded-thumbs='false'";

            $html .= " data-auto-replace='true'";
            $html .= " data-max-file-count='" . $max . "'";
            $html .= " data-allowed-file-extensions='" . json_encode($format) . "'";

            $html .= " data-upload-async='false'";
            $html .= " data-upload-url='" . $this->classUpload . "'";
            $html .= " data-delete-url='" . $this->classUpload . "'";
            $html .= " data-upload-extra-data='" . json_encode($uploadExtraData) . "'";
            $html .= " data-delete-extra-data='" . json_encode($deleteExtraData) . "'";

            $html .= " data-overwrite-initial='false'";
            $html .= " data-prefer-iconic-preview='true'";

            $html .= " data-preview-file-type='any'";
            $html .= " data-preview-file-icon-settings='" . json_encode($previewFileIconSettings) . "'";
            $html .= " data-preview-zoom-button-icons='" . json_encode($previewZoomButtonIcons) . "'";
            $html .= " data-preview-zoom-button-classes='" . json_encode($previewZoomButtonClasses) . "'";

            $html .= " data-initial-preview-as-data='true'";
            $html .= " data-initial-preview='" . json_encode($filesPreview['initialPreview']) . "'";
            $html .= " data-initial-preview-config='" . json_encode($filesPreview['initialPreviewConfig']) . "'";
            $html .= " data-initial-preview-download-url='" . $this->urlHost . "/" . $this->dirUpload . "/" . $modulo . "/" . $id . "/" . $instancia . "/{filename}'";
            $html .= " data-initial-preview-show-delete='true'";

            $html .= " data-purify-html='true'";
            $html .= " data-file-action-settings='" . json_encode($fileActionSettings) . "'";
            $html .= ">";
            $html .= "</div>";

            // ************************
            // JAVASCRIPT *************
            // ************************
            $script = "<script>
                $(document).ready(function(){
                    // HTML del objeto input
                    htmlInput" . $identification . " = $('input[name=\"" . $identification . "[]\"]').parent().html();

                    // Inicializador/Reinicializador de Bootstrap Input File
                    initPlugin" . $identification . " = function(data=false){
                        $('input[name=\"" . $identification . "[]\"]').parent().html(htmlInput" . $identification . ");
                        objetoInput" . $identification . "=$('input[name=\"" . $identification . "[]\"]');

                        // Carga los valores nuevos si es que son pasados desde el servidor
                        if(data!==false){
                            objetoInput" . $identification . ".data('initial-preview', data.initialPreview);
                            objetoInput" . $identification . ".data('initial-preview-config', data.initialPreviewConfig);
                        }

                        objetoInput" . $identification . ".fileinput();

                        // fix para evitar que se congele el número de archivos cargados al eliminar previews de archivos
                        objetoInput" . $identification . ".on('filedeleted', function(){
                            $(this).fileinput('clear');
                        });

                        // Re-Orden
                        objetoInput" . $identification . ".on('filesorted', function(event, params){
                            $.ajax({
                                url: '" . $this->classUpload . "',
                                method: 'post',
                                data: {
                                    uploadFile  : true,
                                    modulo      : '" . $modulo . "',
                                    id          : '" . $id . "',
                                    instancia   : '" . $instancia . "',
                                    accion      : 'order',
                                    data        : params.oldIndex+'-'+params.newIndex
                                },
                                dataType: 'json'
                            }).done(function(data){
                                if(data.detalle){
                                    $.ajax({
                                        url: '" . $this->classUpload . "',
                                        method: 'post',
                                        data: {
                                            uploadFile  : true,
                                            modulo      : '" . $modulo . "',
                                            id          : '" . $id . "',
                                            instancia   : '" . $instancia . "',
                                            accion      : 'load'
                                        },
                                        dataType: 'json'
                                    }).done(function(data){
                                        objetoInput" . $identification . ".fileinput('destroy');
                                        initPlugin" . $identification . "(data);
                                    })
                                }else{
                                    alert('Ocurrio un error al intentar reordenar los archivos. Vuelva a intentarlo en unos minutos.');
                                }
                            }).fail(function(){
                                alert('Ocurrio un error al intentar reordenar los archivos. Vuelva a intentarlo en unos minutos.');
                            })
                        });
                    };
                    initPlugin" . $identification . "();
                });
            </script>";

            return ['html' => $html, 'script' => $script];
        }

        // busca los archivos cargados actualmente en el modulo/instancia
        public function loadFiles($dir, $versiones = null, $format = self::tipos_permitidos){
            $versions = $this->loadVersions($versiones);

            // ARCHIVOS existentes
            $archivos = $this->listDir($dir);

            // CONFIGURACIÓN para cada archivo existente
            $initialPreview = [];
            $initialPreviewConfig = [];
            foreach($archivos as $key => $value){
                $fileNameArray = explode("/", $value);
                $fileName = $fileNameArray[(count($fileNameArray) - 1)];
                $preview = $this->getFileType($fileName);

                // Verifica si se cargaron archivos nuevos por otros medios y los indexa
                $evalString = "/^" . str_repeat("\d", $this->indexDigit) . "_/";
                if(preg_match($evalString, $fileName) == 1){
                    // archivo indexado
                    $filenameNoIndex = substr($fileName, ($this->indexDigit+1));
                }else{
                    // archivo NO indexado
                    $newIndex = $this->getNewIndex($dir);
                    rename($dir . "/" . $fileName, $dir . "/" . $newIndex . "_" . $fileName);
                    $value = $dir . "/" . $newIndex . "_" . $fileName;
                    $filenameNoIndex = $fileName;
                    $fileName = $newIndex . "_" . $fileName;
                }

                $initialPreview[$key] = $this->urlHost . $value;
                $initialPreviewConfig[] = [
                    "caption"       => $filenameNoIndex,
                    "filename"      => $fileName,
                    "filetype"      => mime_content_type($value),
                    "type"          => $preview["type"],
                    "previewAsData" => $preview["previewAsData"],
                    "key"           => $fileName
                ];
            }

            return ["initialPreview" => $initialPreview, "initialPreviewConfig" => $initialPreviewConfig];
        }

        // Upload
        public function fileUpload($dir, $id, $instancia, $maxFiles, $versions = null){
            $versions = $this->loadVersions($versions);

            // Identificador interno del plugin
            $identification = $id . $instancia;
            if(isset($_FILES[$identification])){

                // Verifico que no exceda la cantidad total de archivos cargados+susbidos (0=ilimitado)
                if($maxFiles > 0){
                    $archivosNuevos = count($_FILES[$identification]['name']);
                    $archivosExistentes = count($this->listDir($dir));
                    if(($archivosNuevos + $archivosExistentes) > $maxFiles){
                        $arrayReturn = ["error" => "La cantidad de archivos es superior a la permitida."];
                        return $arrayReturn;
                    }
                }

                // Rearmo el array $_FILES[] para que sea leído correctamente a continuación
                $files = [];
                foreach($_FILES[$identification] as $k => $l){
                    foreach($l as $i => $v){
                        if(!array_key_exists($i, $files)){
                            $files[$i] = [];
                        }
                        $files[$i][$k] = $v;
                    }
                }

                // Proceso cada uno de los archivos
                foreach($files as $archivo){
                $handle = new upload($archivo, "es_ES");

                if($handle->uploaded){
                    $handle->file_overwrite = true;
                    $handle->no_script = false;
                    $dir .= "/";

                    // Solo imágenes
                    if($handle->file_is_image){
                        foreach($versions as $categoria => $opciones){
                            if($categoria == ""){ continue; } // evito el archivo 'original'
                                // Agrego como prefijo del nombre, el número de indice/orden para los archivos nuevos
                                $handle->file_name_body_pre = $this->getNewIndex($dir) . "_";
                                $this->editImg($handle, $opciones); //pre-procesamiento
                                $handle->process($dir . $categoria . "/");
                                $handle->processed;
                            }
                        }

                        // Agrego como prefijo del nombre, el número de indice/orden para los archivos nuevos
                        $handle->file_name_body_pre = $this->getNewIndex($dir) . "_";
                        $handle->process($dir);

                        // verifica que el proceso se termine correctamente
                        if($handle->processed){
                            $urlImg = $this->urlHost . $dir . "/" . $handle->file_dst_name;
                            $preview = $this->getFileType($handle->file_dst_name);
                            $arrayReturn["initialPreview"][] = $urlImg;
                            $arrayReturn["initialPreviewConfig"][] = [
                                "caption"           => substr($handle->file_dst_name, ($this->indexDigit + 1)),
                                "filename"          => $handle->file_dst_name,
                                "filetype"          => $handle->file_src_mime,
                                "type"              => $preview["type"],
                                "previewAsData"     => $preview["previewAsData"],
                                "key"               => $handle->file_dst_name
                            ];
                            $arrayReturn["append"] = true;
                        }else{
                            $arrayReturn = ['error' => "Ocurrio un error al intentar cargar el archivo. (" . $handle->error . ")"];
                        }

                        $handle->clean();
                    }else{
                        $arrayReturn = ["error" => "Ocurrio un error al intentar cargar el archivo (" . $archivo['name'] . ")."];
                    }
                }
            }else{
                $arrayReturn = ["error" => "No se seleccionó ningún archivo para cargar u ocurrio un error al enviarlo."];
            }

            return $arrayReturn;
        }

        // Delete
        public function fileDelete($dir, $key, $versions = null){
            $versions = $this->loadVersions($versions);
            $arrayReturn = [];
            foreach($versions as $directorio => $opciones){
                if($directorio == ""){
                    if(!unlink($dir . "/" . $key)){
                        $arrayReturn = ['error' => 'Error al intentar eliminar el archivo'];
                    }
                }else{
                    $fileName = substr($key, 0, strpos($key, "."));
                    $file = glob($dir . "/" . $directorio . "/" . $fileName . ".*");
                    if(isset($file[0]) && file_exists($file[0])){
                        if(!unlink($file[0])){
                            $arrayReturn = ['error' => 'Error al intentar eliminar el archivo'];
                        }
                    }
                }
            }

            return $arrayReturn;
        }

        // Order
        // el parametro 'data' es enviado con el formato "X-Y" que representa que, el elemento en el orden "X", se movió a la posición "Y"
        public function fileOrder($dir, $data, $versions = null){
            $versions = $this->loadVersions($versions);

            // Capturo los indices (origen-destino)
            $dataExplode = explode("-", $data);
            $indexOrigen = $dataExplode[0];
            $indexDestino = $dataExplode[1];

            // escaneo el directorio por archivos existentes
            $archivos = $this->listDir($dir);

            // Reordenamiento del array
            $this->moveElement($archivos, $indexOrigen, $indexDestino);

            // se renombran todos los archivos
            $inicial = 1;
            $ok = true;
            foreach($archivos as $index => $archivo){
                foreach($versions as $subdirectorio => $opciones){
                    $indiceOriginal = substr(str_replace($dir."/", "", $archivo), 0, $this->indexDigit);
                    $fileNameOrigenNoIndex = substr(str_replace($dir."/", "", $archivo), $this->indexDigit);
                    $fileNameDestino = trim(sprintf("%'.0" . $this->indexDigit . "d\n", $inicial)).$fileNameOrigenNoIndex;
                    if($subdirectorio == ""){
                        $uri = $dir."/";
                    }else{
                        $uri = $dir."/".$subdirectorio."/";
                    }
                    if(file_exists($uri.$indiceOriginal.$fileNameOrigenNoIndex)){
                        if(!rename($uri.$indiceOriginal.$fileNameOrigenNoIndex, $uri.$fileNameDestino)){
                            $ok = false;
                        }
                    }
                }
                $inicial++;
            }

            if($ok){
                return ['detalle' => 'ok'];
            }else{
                return ['detalle' => 'error'];
            }
        }

        // Reordenamiento del array
        private function moveElement(&$array, $a, $b){
            $out = array_splice($array, $a, 1);
            array_splice($array, $b, 0, $out);
        }

        // Editar imagen
        private function editImg($handle, $opImg){
            if(isset($opImg['image_convert'])){ $handle->image_convert = $opImg['image_convert']; } // 'png'|'jpeg'|'gif'|'bmp'
            if(isset($opImg['png_compression'])){ $handle->png_compression = $opImg['png_compression']; } // 1-9
            if(isset($opImg['jpeg_quality'])){ $handle->jpeg_quality = $opImg['jpeg_quality']; } // 1-100

            if(isset($opImg['image_resize'])){
                $handle->image_resize = true;
                if(isset($opImg['image_x'])){ $handle->image_x = $opImg['image_x']; } // 800
                if(isset($opImg['image_y'])){ $handle->image_x = $opImg['image_y']; } // 600
                if(isset($opImg['image_ratio'])){ $handle->image_ratio = true; }
                if(isset($opImg['image_ratio_crop'])){ $handle->image_ratio_crop = true; }
                if(isset($opImg['image_ratio_fill'])){ $handle->image_ratio_fill = true; }
                if(isset($opImg['image_ratio_x'])){ $handle->image_ratio_x = true; }
                if(isset($opImg['image_ratio_y'])){ $handle->image_ratio_y = true; }
                if(isset($opImg['image_no_enlarging'])){ $handle->image_no_enlarging = true; }
                if(isset($opImg['image_no_shrinking'])){ $handle->image_no_shrinking = true; }
            }
        }

        // Obtener tipo de Archivo (propio del "inputFile")
        public function getFileType($archivo){
            $arrayTemp = explode(".", $archivo);

            $type = "other"; // valor por defecto
            $previewAsData = false; // sin visualizador

            $extension = array_pop($arrayTemp);
            switch(strtolower($extension)){
                case "mov":
                case "avi":
                case "mpg":
                case "mkv":
                case "mp4":
                case "3gp":
                case "webm":
                case "wmv":
                    $type = "video";
                    $previewAsData = true;
                    break;

                case "mp3":
                case "m4a":
                case "wav":
                case "ogg":
                    $type = "audio";
                    $previewAsData = true;
                    break;

                case "txt":
                    $type = "text";
                    $previewAsData = true;
                    break;

                case "pdf":
                    $type = "pdf";
                    $previewAsData = true;
                    break;

                case "htm":
                case "html":
                    $type = "html";
                    $previewAsData = true;
                    break;

                case "jpg":
                case "jpeg":
                case "gif":
                case "bmp":
                case "png":
                    $type = "image";
                    $previewAsData = true;
                    break;

            }

            return ["type" => $type, "previewAsData" => $previewAsData];
        }

        // Devuelve el proximo indice a aplicar
        public function getNewIndex($dir){
            // escaneo el directorio por archivos existentes
            $archivos = $this->listDir($dir);

            // Filtro los archivos no indexados (agregados manualmente)
            foreach($archivos as $key => $value){
                $fileName = str_replace($dir."/", "", $value);
                $evalString = "/^" . str_repeat("\d", $this->indexDigit)."_/";
                if(preg_match($evalString, $fileName) == 0){
                    unset($archivos[$key]);
                }
            }

            // busco el nuevo indice
            if(empty($archivos)){
                // en el caso de que sea el primer archivo
                $index = sprintf("%'.0" . $this->indexDigit . "d\n", 1);
            }else{
                // si no es el priemr archivo, busco el proximo indice
                $ultimoArchivo = $archivos[(count($archivos) - 1)];
                $fileName = str_replace($dir."/", "", $ultimoArchivo);
                $fileIndex = substr($fileName, 0, $this->indexDigit);
                $index = sprintf("%'.0" . $this->indexDigit ."d\n", (intval($fileIndex) + 1));
            }

            return trim($index);
        }

        // Determina la cantidad de versiones de una misma imagene que deben crearse/manipularce
        public function loadVersions($versions = null){
            if(is_null($versions)){
                $versions = $this->defaultVersions;
            }else{
                $versions = array_merge($versions, $this->defaultVersions);
            }
            return $versions;
        }

        // Busca todos los directorios antiguos o con errores en la carga (para luego eliminarlos)
        public function trashCollector(){
            $dirsModulos = array_filter(glob($this->dirUpload . '*/*'), 'is_dir');

            // Lista todos los directorios
            foreach($dirsModulos as $key => $value){
                $dirArray = explode("/", $value);
                $dirName = $dirArray[2];

                // Busca los directorios temporales
                if(substr($dirName, 0, 4) == "tmp_"){
                    $fechaDir = substr($dirName, 4, 8);

                    // Fecha anterior a hoy, deber ser eliminado
                    if($fechaDir !== date("Ymd")){
                        $this->deleteDirectory($value);
                    }
                }
            }
        }

        // Elimina los directorios (recursivamente)
        public function deleteDirectory($dir){
            if(!file_exists($dir)){
                return true;
            }

            if(!is_dir($dir)){
                return unlink($dir);
            }

            foreach(scandir($dir) as $item){
                if($item == '.' || $item == '..'){
                    continue;
                }

                if(!$this->deleteDirectory($dir."/".$item)){
                    return false;
                }
            }
            return rmdir($dir);
        }

        // Recupera el listado de archivos cargados para utilizar en el FrontEnd
        public function getFiles($modulo, $id, $instancia = false){
            $files = [];
            if($instancia == false){
                $dir = $this->dirUpload . $modulo . "/" . $id . "/";
                $dirs = array_filter(glob($dir . '*'), 'is_dir');
                foreach($dirs as $subdir){
                    $filesTemp = $this->loadFiles($subdir);
                    foreach($filesTemp['initialPreview'] as $key => $value){
                        $instancia = str_replace($dir, "", $subdir);
                        $files[$instancia][$key]['name'] = $filesTemp['initialPreviewConfig'][$key]['caption'];
                        $files[$instancia][$key]['url'] = $value;
                        $files[$instancia][$key]['type'] = $filesTemp['initialPreviewConfig'][$key]['filetype'];
                        $files[$instancia][$key]['versions'] = $this->getFilesVersions($subdir, $value);
                    }
                }
            }else{
                $dir = $this->dirUpload . $modulo . "/" . $id . "/" . $instancia;
                $filesTemp = $this->loadFiles($dir);
                foreach($filesTemp['initialPreview'] as $key => $value){
                    $files[$key]['name'] = $filesTemp['initialPreviewConfig'][$key]['caption'];
                    $files[$key]['url'] = $value;
                    $files[$key]['type'] = $filesTemp['initialPreviewConfig'][$key]['filetype'];
                    $files[$key]['versions'] = $this->getFilesVersions($dir, $value);
                }
            }

            return $files;
        }

        // Recupera el listado de versiones de un mismo archivos
        private function getFilesVersions($dir, $file){
            $files = [];

            $file = substr($file, (strrpos($file, "/")+1));
            $fileName = substr($file, 0, (strrpos($file, ".")));
            $dirs = array_filter(glob($dir.'/*'), 'is_dir');
            foreach($dirs as $subdir){
                if($list = glob($subdir."/".$fileName.".*")){
                    $files[substr($subdir, (strrpos($subdir, "/") + 1))] = $this->urlHost . $list[0];
                }
            }
            return $files;
        }

        // Lista los archivos disponibles en un directorio
        public function listDir($dir){
            $files = [];

            if(is_dir($dir) && $handle = opendir($dir)){
                while(false !== ($entry = readdir($handle))){
                    if($entry != "." AND $entry != ".." AND !is_dir($dir . "/" . $entry)){
                        $files[] = $dir . "/" . $entry;
                    }
                }
                closedir($handle);
            }

            sort($files);
            return $files;
        }

        // This is how we scan directories
        private function find_contents($dir){
            $result=[];
            $root=scandir($dir);
            foreach($root as $value){
                if($value === '.' || $value === '..') {continue;}
                if(is_file($dir . $this->directory_separator . $value)){
                    if(!$this->ext_filter || in_array(strtolower(pathinfo($dir . $this->directory_separator . $value, PATHINFO_EXTENSION)), $this->ext_filter)){
                        $this->files[] = $result[] = $dir . $this->directory_separator . $value;
                    }
                    continue;
                }
                if($this->recursive){
                    foreach($this->find_contents($dir . $this->directory_separator . $value) as $value){
                        $this->files[] = $result[] = $value;
                    }
                }
            }
            // Return required for recursive search
            return $result;
        }

    }