<?php
  // Require de la clase y PlugIn
  require_once("class/class.upload.php/lang/class.upload.es_ES.php");
  require_once("class/class.upload.php/class.upload.php");
  require_once("upload.files.php");

  // Captura los valores enviados por POST
  foreach($_POST as $post_key => $post_value){
    $$post_key=$post_value;
  }

  // Controlador
  $arrayReturn=['error' => 'Ocurrio un error inesperado.'];
  $versiones=[
    "prueba" => false,
    "otro" => false,
    "test" => false
  ];

  if(isset($instancia) AND isset($accion)){
    $dir = uploadFiles::$dirUpload.$modulo."/".$id."/".$instancia;
    switch($accion){
      case "load":
        $arrayReturn=uploadFiles::loadFiles($dir, $versiones);
        break;

      case "add":
        $arrayReturn=uploadFiles::fileUpload($dir, $id, $instancia, $maxFiles, $versiones);
        break;

      case "del":
        $arrayReturn=uploadFiles::fileDelete($dir, $key, $versiones);
        break;

      case "order":
        $arrayReturn=uploadFiles::fileOrder($dir, $data, $versiones);
        break;

      default:
        $arrayReturn=array('error' => 'Funcion incorrecta.');
        break;

    }

    echo json_encode($arrayReturn);
    exit;
  }
